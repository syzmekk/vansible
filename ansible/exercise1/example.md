# Exercise 1

## CentOS:

### Install dependencies

```sh
sudo yum install epel-release python-pip -y
```

### Install Ansible

```sh
sudo pip install ansible -y
```

### Install & extract Ansible mitogen

```sh
wget -c https://files.pythonhosted.org/packages/source/m/mitogen/mitogen-0.2.5.tar.gz -O - | tar -xz
```

### Move it to /etc/ansible/ && update ownership

```sh
sudo mv mitogen-0.2.5/ /etc/ansible/ && sudo chown root:root /etc/ansible/mitogen-0.2.5/
```

### Edit /etc/ansible/ansible.cfg file

```sh
sudo nano /etc/ansible/ansible.cfg
```

and add

```ini
[defaults]
strategy_plugins = /etc/ansible/mitogen-0.2.5/ansible_mitogen/plugins/strategy
strategy = mitogen_linear
```

### Add hosts to /etc/ansible/hosts file

```sh
sudo nano /etc/ansible/hosts
```

and add

```ini
[example] ; define group
hostname1 ; define hostnames or ips
hostname2 ; make sure that your instance resolves hostnames properly
127.0.0.1
other_ip
```

### Exchange SSH keys
For example:

```sh
ssh-copy-id user@remote
```

or

```sh
cat ~/.ssh/id_rsa.pub | ssh user@remote "mkdir -p ~/.ssh && cat >> ~/.ssh/authorized_keys"
```

### Execute ansible ping

```sh
ansible -m ping all
```

## Ubuntu:

### Add repository & update the package cache

```sh
sudo apt-add-repository ppa:ansible/ansible -y -u
```

### Install Ansible

```sh
sudo apt install ansible -y
```

### Install & extract Ansible mitogen

```sh
wget -c https://files.pythonhosted.org/packages/source/m/mitogen/mitogen-0.2.5.tar.gz -O - | tar -xz
```

### Move it to /etc/ansible/ && update ownership

```sh
sudo mv mitogen-0.2.5/ /etc/ansible/ && sudo chown root:root /etc/ansible/mitogen-0.2.5/
```

### Edit /etc/ansible/ansible.cfg file

```sh
sudo nano /etc/ansible/ansible.cfg
```

and add

```ini
[defaults]
strategy_plugins = /etc/ansible/mitogen-0.2.5/ansible_mitogen/plugins/strategy
strategy = mitogen_linear
```

### Add hosts to /etc/ansible/hosts file

```sh
sudo nano /etc/ansible/hosts
```

and add

```ini
[example] ; define group
hostname1 ; define hostnames or ips
hostname2 ; make sure that your instance resolves hostnames properly
127.0.0.1
other_ip
```

### Exchange SSH keys
For example:

```sh
ssh-copy-id user@remote
```

or

```sh
cat ~/.ssh/id_rsa.pub | ssh user@remote "mkdir -p ~/.ssh && cat >> ~/.ssh/authorized_keys"
```

### Execute ansible ping

```sh
ansible -m ping all
```
