# Exercise 10

## Ubuntu:

### CLI

```sh
# Install python pip
sudo apt install python-pip -y

# Upgrade pip
pip install --upgrade pip

# Install virtualenv Python module
pip install virtualenv
# or
python -m pip install virtualenv

# Create the virtual environment
virtualenv my_env
# or
python -m virtualenv my_env

# Activate your enviroment
source my_env/bin/activate

# You are now inside that environment & all your actions are restricted to it
# example: (my_env) root@ubuntu-xenial:~#

# Install molecule and docker Python libraries
pip install molecule docker
# or
python -m pip install molecule docker

# Create a new role
molecule init role -r my_role -d docker
# -r specifies role name while -d specifies the driver, which provisions the hosts for molecule, in our case it is docker

# Change into my_role dir
cd my_role/

# Test molecule
molecule test

###########################################################################
#                           EXPECTED OUTPUT                               #
###########################################################################
#--> Validating schema /home/vagrant/my_role/molecule/default/molecule.yml.
#Validation completed successfully.
#--> Test matrix
#
#└── default
#    ├── lint
#    ├── destroy
#    ├── dependency
#    ├── syntax
#    ├── create
#    ├── prepare
#    ├── converge
#    ├── idempotence
#    ├── side_effect
#    ├── verify
#    └── destroy

```

### Tests

Now you can write/copy your role into *my_env*, write some test cases in Python, specify custom platform, scenarios, and run tests.

Then, remove your virtualenv. Just simply remove whole directory.

```sh
rm -rf my_env/
```

#### Help

- For more information, visit [Jeff Geerling's blog](https://www.jeffgeerling.com/blog/2018/testing-your-ansible-roles-molecule) & [Molecule docs](https://molecule.readthedocs.io/en/latest/).

- Feel free to ask any questions. :grey_question: :wink: