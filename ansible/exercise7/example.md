# Exercise 7

## Both (Ubuntu & CentOS):

### Playbook

```yaml
---
- name: Example playbook
  hosts: all
  become: yes

  tasks:
    - name: Edit hosts file
      lineinfile:
        path: /etc/hosts
        state: present
        line: "127.0.1.1 {{ hostvars[inventory_hostname]['ansible_nodename'] }}"
        regexp: '127\.0\.1\.1.+'

    - name: Edit nginx config file
      lineinfile:
        path: /etc/nginx/nginx.conf
        state: present
        line: "#comment"
        insertbefore: '^user'

    - name: Add env variable
      lineinfile:
        path: /etc/environment
        state: present
        insertafter: EOF
        line: "FOO=bar"

    - name: Add alias
      lineinfile:
        path: "{{ lookup('env', 'HOME') }}/.bashrc"
        state: present
        line: '''alias myinfo='$USER, $SHELL''''
        insertafter: EOF
        regexp: 'alias.myinfo.+'


```