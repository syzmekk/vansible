# Ansible exercise #9
Role 2 - Galaxy

## Objective
- Search for apache2 role
- Install it
- Customize it on your own
- Try to run it
- Debug it (if necessary)

Use (centos or ubuntu) vagrantfiles from Ansible exercise #2