# Exercise 5

## Both (Ubuntu & CentOS):

### Create simple YAML list

For example vars.yml

```yaml
---
files:
  - test.txt
  - foo.bar
  - d.xc
  - dt.u

packages:
  - net-tools
  - unzip
  - zip
  - mc

```

### Create playbook

```yaml
---
- name: Example playbook
  hosts: all
  become: yes

  tasks:
    - name: Include vars
      include_vars: vars.yml

    - name: Install packages - Ubuntu
      apt:
        name: "{{ item }}"
        state: present
      when: ansible_distribution == 'Ubuntu'
      with_nested:
        - "{{ packages }}"

    - name: Install packages - CentOS
      yum:
        name: "{{ item }}"
        state: present
      when: ansible_distribution == 'CentOS'
      with_nested:
        - "{{ packages }}"

    - name: Create files
      file:
       path: "{{ lookup('env', 'HOME') }}/{{ item }}"
       state: present
      with_nested:
        - "{{ files }}"

```
