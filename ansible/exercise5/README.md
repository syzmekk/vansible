# Ansible exercise #5
Loops and conditionals

## Objective
- parse the attached YAML file with list of files that needs to be created
- parse the attached YAML file with list of packages that needs to be installed
- register variable to check whether all nodes have the updated banner (from example 4) installed
- deploy updated banner file only to hosts that doesn't have updated banner in place

Use (centos or ubuntu) vagrantfiles from Ansible exercise #2 and playbooks from Ansible example #4