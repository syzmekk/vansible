# Exercise 2

## Both (Ubuntu & CentOS):

### Define groups and hosts

```sh
sudo nano /etc/ansible/hosts
```

then split

```ini
[master] ; group
hostname1 dw_dir=/tmp/ ; hostname & variable

[slave]
hostname2 dw_dir=/opt/
```

### Playbook

```yaml
---
- name: Example playbook
  hosts: all # define provisioned host(s)
  become: no # do NOT use sudo unnecessarily

  tasks:
    - name: Download file
      get_url:
        url: http://www.ovh.net/files/10Mb.dat
        dest: "{{ dw_dir }}"

```
