# Exercise 8

## Both (Ubuntu & CentOS):

### CLI

```sh
# Init your role dir tree
ansible-galaxy init role_name

# Change your dir into your role
cd role_name/

# Write YAMLs!

```

#### Help

- For conditionals, handlers, files, templates check previous exercises.

- You can help yourself by seeing the [docs](https://docs.ansible.com/ansible/latest/user_guide/playbooks_reuse_roles.html).

- Feel free to ask any questions. :grey_question: :wink: