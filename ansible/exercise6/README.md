# Ansible exercise #6
Reports with facts

## Objective
- Print _node facts_ using debug module
- Get nginx version and register it as _version_
- Create a report.csv file, include:
    - hostname,
    - distribution,
    - IP address,
    - time,
    - nginx version.

Use (centos & ubuntu) vagrantfiles from Ansible exercise #2