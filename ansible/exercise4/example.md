# Exercise 4

## Both (Ubuntu & CentOS):

### Create simple HTML file

```html
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Ansible exercise #4</title>
    <meta charset="utf-8">
    <style type="text/css">
        span {
            width: 1176px;
            height: 371px;
            background-image: url("https://www.ansible.com/hubfs/red-hat-ansible.svg");
            background-color: rgba(0, 0, 0, 0.2);
            position: absolute;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            margin: auto;
        }
    </style>
</head>

<body>
    <span></span>
</body>

</html>
```

### Deploy custom HTML file

```yaml
---
- name: Example playbook
  hosts: # node 2 & 3
  become: yes

  tasks:
    - name: Check if index.nginx-debian.html exists
      stat:
        path: /var/www/html/index.nginx-debian.html
      register: index_ubuntu
      when: ansible_distribution == 'Ubuntu'

    - name: Check if index.html exists
      stat:
        path: /usr/share/nginx/html/index.html
      register: index_centos
      when: ansible_distribution == 'CentOS'

    - name: Remove old default file - Ubuntu
      file:
        path: /var/www/html/index.nginx-debian.html
      when: (ansible_distribution == 'Ubuntu') and (index_ubuntu.stat.exists == true)

    - name: Remove old default file - CentOS
      file:
        path: /usr/share/nginx/html/index.html
      when: (ansible_distribution == 'CentOS') and (index_centos.stat.exists == true)

    - name: Deploy HTML file
      template:
        src: # src path
        dest: "{{ '/var/www/html/index.html' if ansible_distribution == 'Ubuntu' else '/usr/share/nginx/html/index.html' }}"
        owner: root
        group: root
        mode: 0644

```

### Create template nginx.conf with variables

```nginx
user {{ user }};
worker_processes auto;
pid /run/nginx.pid;

events {
        worker_connections 768;
        # multi_accept on;
}

http {

        ##
        # Basic Settings
        ##

        sendfile on;
        tcp_nopush on;
        tcp_nodelay on;
        keepalive_timeout 65;
        types_hash_max_size 2048;
        # server_tokens off;

        # server_names_hash_bucket_size 64;
        # server_name_in_redirect off;

        include /etc/nginx/mime.types;
        default_type application/octet-stream;

        ##
        # SSL Settings
        ##

        ssl_protocols TLSv1 TLSv1.1 TLSv1.2; # Dropping SSLv3, ref: POODLE
        ssl_prefer_server_ciphers on;

        ##
        # Logging Settings
        ##

        access_log /var/log/nginx/access.log;
        error_log /var/log/nginx/error.log;

        ##
        # Gzip Settings
        ##

        gzip on;
        gzip_disable "msie6";

        # gzip_vary on;
        # gzip_proxied any;
        # gzip_comp_level 6;
        # gzip_buffers 16 8k;
        # gzip_http_version 1.1;
        # gzip_types text/plain text/css application/json application/javascript text/xml application/xml application/xml+rss text/javascript;

        ##
        # Virtual Host Configs
        ##

        include /etc/nginx/conf.d/*.conf;
        include {{ sites_path }}/*;
}
```

### Update nginx config

```yaml
---
- name: Example playbook
  hosts: # node 1
  become: yes

  vars:
    user: www-data
    sites_path: /etc/nginx/sites-enabled

  tasks:
    - name: Remove old config
      file:
        path: /etc/nginx/nginx.conf
        state: absent

    - name: Add custom config
      template:
        src: # template source path, relative or absolute
        dest: /etc/nginx/nginx.conf
        owner: root
        group: root
        mode: 0644
      notify:
        - Restart nginx

  handlers:
    - name: Restart nginx
      become: yes
      service:
        name: nginx
        state: restarted

```
