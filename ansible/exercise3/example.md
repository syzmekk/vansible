# Exercise 3 (CLI)

## Both (Ubuntu & CentOS):

### Download file to master host

```sh
ansible -m get_url -a "url=http://www.ovh.net/files/10Mb.dat dest=/tmp" master
```

- _-m_ stands for module
- _-a_ stands for args

### Send to all other nodes

```sh
ansible -m copy -b -a "src=/tmp/10Mb.dat dest=/opt/10Mb.dat" all
```

- _-b_ stands for _become_ (sudo user in our case)

### Add user test

```sh
ansible -m user -b -a "name=test" all
```

### Download file as ansible

```sh
ansible -m get_url --become-user=ansible -a "url=http://www.ovh.net/files/10Mb.dat dest={{ lookup('env', 'HOME') }}" all
```

### Remove htop package

```sh
# Ubuntu
ansible -m apt -b -a "name=htop state=absent" all

# CentOS
ansible -m yum -b -a "name=htop state=absent" all
```

### Remove user test

```sh
ansible -m user -b -a "name=test state=absent" all
```

### Stop nginx & use curl

```sh
ansible -m service -b -a "name=nginx state=stopped" all

ansible -m uri -a "url=localhost" all # GET request, will return 200 code if NOT stopped - run in verbose mode (-vvv)

```

### Reboot nodes

```sh
ansible -m reboot -b all
```
