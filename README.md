# Vansible
Repository for DevOps Tools 101

## Prerequisites
In order to begin your training you need to install [Chocolatey package manager](https://chocolatey.org/) and then using chocolatey you have to install: 
- [Git for windows](https://chocolatey.org/packages/git.install)
- [Vagrant](https://chocolatey.org/packages/vagrant/)
- [Virtualbox](https://chocolatey.org/packages/virtualbox)
- [Visual Studio Code](https://chocolatey.org/packages/vscode)

and last but not least your favourite terminal emulator. We recommend using [ConEmu](https://chocolatey.org/packages/ConEmu)

## Contents
This repository contains exercises for:
- [Vagrant](https://www.vagrantup.com/)
- [Ansible](https://www.ansible.com/)

### Repository organization
The repository follows certain Standard Directory Layout.
```
/
/ansible/
/ansible/exercise1/
/ansible/execrise2/
/vagrant/
/vagrant/exercise1/
/vagrant/exercise2/
```

in each of exercise folders you will get file called README.md that will contain execrcise descriptions. 

### How should i submit my answers?
You should clone this repository, checkout master branch, then create your own branch using the pattern `trainings/NameSurname`
And submit your answers to that branch.




